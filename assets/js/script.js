
$("#front").submit(function(event) {
	$.ajax({
		url: $(this).attr('action'),
		type: 'POST',
		data: $(this).serialize(),
		success: function(data){
			var resp=jQuery.parseJSON(data);
			if (resp.status==true) {
				$("#submit").prop('disabled', 'true');
				$("#submit").val(resp.message);
			}else{
				$("#submit").val('Submit');
			}
		}
	})
	.done(function() {
		console.log("success");
	})
	.fail(function() {
		console.log("error");
	})
	.always(function() {
		console.log("complete");
	});
	return false;
});

