<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();

		$this->load->helper(array('form', 'url'));
		$this->load->library('Session');
		$this->load->library('Form_validation');
		$this->load->library('Tank_auth');
		$this->lang->load('Tank_auth');
	}

	public function index()
	{
		if (!$this->tank_auth->is_logged_in()) {
			redirect('/auth/login/');
		} else {
			//$data['user_id']	= $this->tank_auth->get_user_id();
			//$data['username']	= $this->tank_auth->get_username();
			$user_data = $this->users->get_user_by_id($this->tank_auth->get_user_id(),1);
			$data['firstname'] = $user_data->firstname;
			$data['lastname'] = $user_data->lastname;
			
			if( !$this->session->userdata('image') == '' ){
			$data['image'] = $this->session->userdata('image');
			}else{
			$data['image'] = '/images/blank_man.gif';
			}
			
			$this->load->view('welcome', $data);
			//echo $this->session->userdata('user_id');
		}
	}

}

/* End of file Dashboard.php */
/* Location: ./application/controllers/Dashboard.php */