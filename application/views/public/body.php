<?php $this->load->view('public/header'); ?>
<section class="bg-primary" id="about">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2 text-center">
                    <h2 class="section-heading">We've got what you need!</h2>
                    <hr class="light">
                    <p class="text-faded">make your wedding easier to find, scheduled, many colleagues who came with ease in dateng.in</p>
                    <a href="#" class="btn btn-default btn-xl">Get Started!</a>
                </div>
            </div>
        </div>
    </section>

    <section id="services">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading">Our Work</h2>
                    <hr class="primary">
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-6 text-center">
                    <div class="service-box">
                        <i class="fa fa-4x fa-calendar wow bounceIn text-primary"></i>
                        <h3>Schedule</h3>
                        <p class="text-muted">Scheduling with facebook and google calendar for your connection</p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 text-center">
                    <div class="service-box">
                        <i class="fa fa-4x fa-pencil-square wow bounceIn text-primary" data-wow-delay=".1s"></i>
                        <h3>Testimony about your wedding</h3>
                        <p class="text-muted">Make your friend gives the impression during the wedding</p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 text-center">
                    <div class="service-box">
                        <i class="fa fa-4x fa-camera-retro wow bounceIn text-primary" data-wow-delay=".2s"></i>
                        <h3>Photobooth session</h3>
                        <p class="text-muted">They will gather, selfie, filled the room to take pictures instantly</p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 text-center">
                    <div class="service-box">
                        <i class="fa fa-4x fa-heart wow bounceIn text-primary" data-wow-delay=".3s"></i>
                        <h3>Popular</h3>
                        <p class="text-muted">Make everyone know your wedding</p>
                    </div>
                </div>
            </div>
        </div>
    </section>

   

    <aside class="bg-dark">
        <div class="container text-center">
            <div class="call-to-action">
                <h2>You will try?</h2>
                <form action="<?=base_url()?>api/add" method="POST" role="form" id="front">
                	<legend></legend>
                
                	<div class="form-group">
                		<input type="email" name="email" class="wow btn-xl" style="border-radius: 50px;color:#000000" id="" placeholder="your@email.com" required>
                		<input type="submit" class="btn btn-default btn-xl wow tada" id="submit" value="Submit">
                	</div>
                
                </form>
                <!-- <a href="#" class="btn btn-default btn-xl wow tada">Download Now!</a> -->
            </div>
        </div>
    </aside>

    <section id="contact">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2 text-center">
                    <h2 class="section-heading">Let's Get In Touch!</h2>
                    <hr class="primary">
                    <p>Ready to start for your wedding organizer? That's great! Give us a call or send us an email and we will get back to you as soon as possible!</p>
                </div>
                <!-- <div class="col-lg-8 col-lg-offset-2 text-center">
                    <i class="fa fa-phone fa-3x wow bounceIn"></i>
                    <p>123-456-6789</p>
                </div> -->
                <div class="col-lg-8 col-lg-offset-2 text-center">
                    <i class="fa fa-envelope-o fa-3x wow bounceIn" data-wow-delay=".1s"></i>
                    <p><a href="mailto:info@dateng.in">info@dateng.in</a></p>
                </div>
            </div>
        </div>
    </section>

<?php $this->load->view('public/footer'); ?>
  
